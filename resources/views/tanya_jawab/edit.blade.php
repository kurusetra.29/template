@extends('adminlte.master')

@section('content')
<div class= "mt-2 ml-4 mr-2">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan {{ $tanya->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/pertanyaan/{{$tanya->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text"  class="form-control" id="judul" name="judul" value ="{{ old('judul', $tanya->judul) }}" placeholder="Tulis Judul">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value ="{{ old('isi', $tanya->isi) }}"placeholder="Tulis Isi Pertanyaan">
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                </div>

                
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>

</div>
@endsection

