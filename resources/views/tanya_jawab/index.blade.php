@extends('adminlte.master')

@section('content')
<div class= mt-2 ml-3>
<!-- List Tanya jawab -->
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if (session('sukses'))
                <div class="alert alert-success">
                  {{ session('sukses') }}
                </div>
              @endif
              <a class="btn btn-primary mb-2" href="/pertanyaan/create">Tambah Data </a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 3%">#</th>
                      <th style="width: 20%">Judul</th>
                      <th style="width: 57%">Isi</th>
                      <th style="width: 20%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($tanya as $key => $pertanyaan)
                        <tr>
                        <td> {{ $key + 1}} </td>
                        <td> {{ $pertanyaan->judul }} </td>
                        <td> {{ $pertanyaan->isi }} </td>
                        <td style="display:flex">
                          <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">Tampil</a>
                          <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                          <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                            @csrf 
                            @method('Delete')
                            <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                          </form>
                        </td>
                        </tr>
                    @empty
                        <tr>
                        <td colspan=4> Tidak Ada Data </td>
                        </tr>
                    @endforelse
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>

</div>
@endsection
