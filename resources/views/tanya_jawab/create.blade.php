@extends('adminlte.master')

@section('content')
<div class= "mt-2 ml-4 mr-2">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Input Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text"  class="form-control" id="judul" name="judul" value ="{{ old('judul', '') }}" placeholder="Tulis Judul">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value ="{{ old('isi', '') }}"placeholder="Tulis Isi Pertanyaan">
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                </div>

                <!-- <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <div class="input-group">
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                    <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                    </div>
                </div>
                </div>
                <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Input</button>
            </div>
        </form>
    </div>

</div>
@endsection

