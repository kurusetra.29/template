<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    //
    
    public function create () {
        return view('tanya_jawab.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $request->validate ([
            "judul"=>"required|unique:pertanyaan",
            "isi"=>"required"
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul"=> $request["judul"],
            "isi"=> $request["isi"]
        ]);
        return redirect ('/pertanyaan')->with('sukses', 'Data berhasil diinput');
    }
    public function index () {
        $tanya = DB::table('pertanyaan')->get();
        // dd($tanya);
        return view('tanya_jawab.index', compact ('tanya'));
    }
    public function show ($pertanyaan_id){
        $tanya = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('tanya_jawab.show', compact('tanya'));
    }
    public function edit ($pertanyaan_id) {
        $tanya = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('tanya_jawab.edit', compact('tanya'));
    }
    public function update($pertanyaan_id, Request $request){
        // dd($request->all());
        // $request->validate ([
        //     "judul"=>"required|unique:pertanyaan",
        //     "isi"=>"required"
        // ]);
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->update([
            "judul"=> $request["judul"],
            "isi"=> $request["isi"]
        ]);
        return redirect ('/pertanyaan')->with('sukses', 'Data berhasil DiUPDATE');
    }
    public function destroy ($pertanyaan_id) {
        $tanya = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect ('/pertanyaan')->with('sukses', 'Data berhasil diHAPUS');
    }

}
